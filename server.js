// import
require('dotenv').config()
const express = require('express')
const jwt = require('jsonwebtoken')

// initialize
const app = express()
const port = 3000

// function: verify token and pass token data
const auth = (req, res, next) => {
  // get token from header
  const token = req.header('auth-token')

  // statement: check if token is not empty
  if (token === undefined) return res.status(401).send('Access denied')

  try {
    // verify token
    const verified = jwt.verify(token, process.env.TOKEN_SECRET_KEY)
    // set verified token to req session
    req.bodyData = verified

    next()
  } catch (err) {
    res.status(400).send('Invalid token')
  }
}

// route: locked route
app.get('/secret', auth, (req, res) => {
  res.status(200).send(req.bodyData)
})

// route: create token
app.get('/token', (req, res) => {
  // create and assign token
  const token = jwt.sign({ body: 'dummy' }, process.env.TOKEN_SECRET_KEY)
  // set token to header and send token to frontend
  res.header('auth-token', token).send(token)
})

// start server
app.listen(port, () => {
  console.log(`API is running on port ${port}`)
})
